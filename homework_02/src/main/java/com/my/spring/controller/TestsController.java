package com.my.spring.controller;

import com.my.spring.service.TestsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TestsController {
    @Autowired
    TestsService testsService;

    @GetMapping("/tests")
    public String getTestsPage(@RequestParam(required = false) String currentPage, Model model) {
        testsService.getTests(model, currentPage);
        return "tests";
    }


    @GetMapping("/info")
    public String getTestInfo(@RequestParam int testId, Model model) {
        testsService.getTest(testId, model);
        return "testinfo";
    }

    @GetMapping("/addtest")
    public String getAddTestPage() {
        return "addtest";
    }

    @PostMapping("/addtest")
    public String addNewTest(@RequestParam String testName,
                             @RequestParam String subject,
                             @RequestParam String difficulty,
                             @RequestParam int timeLimitInMinutes,
                             @RequestParam String description) {
        int id = testsService.addNewTest(testName, subject, difficulty, timeLimitInMinutes, description);
        return "redirect:/testedit?testId=" + id;
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping("/deletetest")
    public String deleteTest(@RequestParam int testId) {
        testsService.deleteTest(testId);
        return "redirect:/tests";
    }

    @GetMapping("/testedit")
    public String getTestEditPage(@RequestParam int testId, Model model) {
        testsService.getTest(testId, model);
        return "testedit";
    }

    //HTML does not support HTTP PATCH METHOD.
    @PostMapping("/testedit")
    public String updateTest(@RequestParam int testId,
                             @RequestParam String testName,
                             @RequestParam String subject,
                             @RequestParam String difficulty,
                             @RequestParam int timeLimitInMinutes,
                             @RequestParam String description,
                             @RequestParam(required = false) String submit,
                             Model model) {

        testsService.updateTest(testId, testName, subject, difficulty, timeLimitInMinutes, description, submit);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/addquestion")
    public String addQuestion(@RequestParam String questionText,
                              @RequestParam int testId,
                              Model model) {
        testsService.addQuestionToTest(questionText, testId);

        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/editquestion")
    public String editQuestion(@RequestParam int questionId,
                               @RequestParam String questionText,
                               @RequestParam int testId,
                               Model model) {
        testsService.editQuestion(questionId, questionText);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping("/deletequestion")
    public String deleteQuestion(@RequestParam int questionId,
                                 @RequestParam int testId,
                                 Model model) {
        testsService.deleteQuestion(questionId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/addanswer")
    public String addAnswer(@RequestParam String answerText,
                            @RequestParam boolean correct,
                            @RequestParam int questionId,
                            @RequestParam int testId,
                            Model model) {
        testsService.abbAnswerToQuestion(answerText, correct, questionId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/editanswer")
    public String editAnswer(@RequestParam int answerId,
                             @RequestParam boolean correct,
                             @RequestParam String answerText,
                             @RequestParam int testId,
                             Model model) {
        testsService.editAnswer(answerId, correct, answerText);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping
    public String deleteAnswer(@RequestParam int answerId,
                               @RequestParam int testId,
                               Model model) {
        testsService.deleteAnswer(answerId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }


}
