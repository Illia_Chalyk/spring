package com.my.spring.bpps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
public class TimedHandlingBeanPostProcessor implements BeanPostProcessor {
    Map<String, Class> map = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Timed.class)) {
            map.put(beanName, beanClass);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = map.get(beanName);
        if (Objects.nonNull(beanClass)) {
            Logger log = LoggerFactory.getLogger(beanClass);
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), (proxy, method, args) -> {
                long start = System.nanoTime();
                Object returnedVal = method.invoke(bean, args);
                long end = System.nanoTime();
                log.info("Execution of {}#{} took {} ns", bean.getClass().getName(), method.getName(), (end - start));
                return returnedVal;
            });
        }
        return bean;
    }
}
