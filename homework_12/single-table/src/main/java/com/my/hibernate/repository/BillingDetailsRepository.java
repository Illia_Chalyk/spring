package com.my.hibernate.repository;

import com.my.hibernate.entity.BillingDetails;

import java.util.List;
import java.util.Optional;

public interface BillingDetailsRepository {
    void save(BillingDetails billingDetails);

    void saveAll(List<BillingDetails> list);

    Optional<BillingDetails> get(Long buyerId);

    List<BillingDetails> getAll(Long buyerId);

    void delete(Long id);
}
