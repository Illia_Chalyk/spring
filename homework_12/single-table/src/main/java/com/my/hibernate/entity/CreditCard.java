package com.my.hibernate.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@DiscriminatorValue("CREDIT_CARD")
public class CreditCard extends BillingDetails {
    String cardNumber;
    int expYear;
    int expMonth;
}
