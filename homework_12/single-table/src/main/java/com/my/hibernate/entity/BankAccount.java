package com.my.hibernate.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
@DiscriminatorValue("BANK_ACCOUNT")
public class BankAccount extends BillingDetails {
    String account;
    String bankName;
}
