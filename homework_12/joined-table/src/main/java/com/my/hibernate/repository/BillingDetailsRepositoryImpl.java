package com.my.hibernate.repository;

import com.my.hibernate.entity.BillingDetails;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public class BillingDetailsRepositoryImpl implements BillingDetailsRepository {
    protected final SessionFactory sessionFactory;

    @Autowired
    public BillingDetailsRepositoryImpl(@NonNull SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void save(@NonNull BillingDetails billingDetails) {
        sessionFactory.getCurrentSession().save(billingDetails);
    }

    @Override
    @Transactional
    public void saveAll(@NonNull List<BillingDetails> list) {
        Session session = sessionFactory.getCurrentSession();
        for (BillingDetails billingDetails : list) {
            session.save(billingDetails);
        }
    }

    @Override
    @Transactional
    public Optional<BillingDetails> get(@NonNull Long buyerId) {
        BillingDetails billingDetails = sessionFactory.getCurrentSession().get(BillingDetails.class, buyerId);
        return Optional.of(billingDetails);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<BillingDetails> getAll(@NonNull Long buyerId) {
        List<BillingDetails> billingDetails = sessionFactory
                .getCurrentSession()
                .getNamedQuery("BillingDetails_getByBuyerId")
                .setParameter("buyerId", buyerId)
                .getResultList();
        return billingDetails;
    }

    @Override
    @Transactional
    public void delete(@NonNull Long id) {
        Session session = sessionFactory.getCurrentSession();
        BillingDetails billingDetails = session.get(BillingDetails.class, id);
        session.delete(billingDetails);
    }
}
