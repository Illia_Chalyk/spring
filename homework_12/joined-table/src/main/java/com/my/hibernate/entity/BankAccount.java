package com.my.hibernate.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class BankAccount extends BillingDetails {
    String account;
    String bankName;
}
