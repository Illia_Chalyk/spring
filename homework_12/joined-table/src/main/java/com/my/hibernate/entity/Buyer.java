package com.my.hibernate.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Buyer {
    @Id
    @GeneratedValue
    Long id;
    String firstName;
    String lastName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "buyer", orphanRemoval = true)
    List<BillingDetails> billingDetails = new ArrayList<>();
}
