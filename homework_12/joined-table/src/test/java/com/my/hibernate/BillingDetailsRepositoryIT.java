package com.my.hibernate;

import com.my.hibernate.cfg.HibernateConfiguration;
import com.my.hibernate.entity.BankAccount;
import com.my.hibernate.entity.BillingDetails;
import com.my.hibernate.entity.Buyer;
import com.my.hibernate.entity.CreditCard;
import com.my.hibernate.repository.BillingDetailsRepository;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@SpringJUnitConfig(HibernateConfiguration.class)
public class BillingDetailsRepositoryIT {
    @Autowired
    BillingDetailsRepository sut;

    @Test
    public void shouldCreateApplicationContext(ApplicationContext context) {
        Assertions.assertNotNull(context);
    }

    @Test
//    @Transactional
    public void shouldLoadAllBillingDetails() {
        //GIVEN
        Buyer buyer = getMockBuyer();
        List<BillingDetails> expected = getMockBillingDetails(buyer);
        sut.saveAll(expected);
        //WHEN
        List<BillingDetails> actual = sut.getAll(buyer.getId());
        //THEN
        Assertions.assertEquals(expected, actual);
    }

    private List<BillingDetails> getMockBillingDetails(Buyer buyer) {
        List<BillingDetails> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(getMockBankAccount(buyer));
            list.add(getMockCreditCard(buyer));
        }
        buyer.setBillingDetails(list);
        return list;
    }

    private CreditCard getMockCreditCard(Buyer buyer) {
        CreditCard creditCard = new CreditCard();
        creditCard.setCardNumber(generateRandomCardNumber());
        creditCard.setExpMonth((int) (Math.random() * (12 - 1) + 1));
        creditCard.setExpYear((int) (Math.random() * (2100 - 2021) + 2021));
        creditCard.setBuyer(buyer);
        return creditCard;
    }

    private String generateRandomCardNumber() {
        StringBuilder number = new StringBuilder();
        number.append(((int) (Math.random() * (10_000 - 1000) + 1000)))
                .append("-")
                .append(((int) (Math.random() * (10_000 - 1000) + 1000)))
                .append("-")
                .append(((int) (Math.random() * (10_000 - 1000) + 1000)))
                .append("-")
                .append(((int) (Math.random() * (10_000 - 1000) + 1000)));
        return number.toString();
    }

    private Buyer getMockBuyer() {
        Buyer buyer = new Buyer();
        buyer.setFirstName(RandomString.make());
        buyer.setLastName(RandomString.make());
        return buyer;
    }

    private BankAccount getMockBankAccount(Buyer buyer) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccount(RandomString.make());
        bankAccount.setBankName(RandomString.make());
        bankAccount.setBuyer(buyer);
        return bankAccount;
    }

}
