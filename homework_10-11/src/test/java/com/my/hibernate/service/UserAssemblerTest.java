package com.my.hibernate.service;

import com.my.hibernate.dto.QuizResultDto;
import com.my.hibernate.dto.UserDto;
import com.my.hibernate.entity.Quiz;
import com.my.hibernate.entity.QuizResult;
import com.my.hibernate.entity.User;
import com.my.hibernate.repository.QuizRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.UUID;

public class UserAssemblerTest {
    private final QuizRepository quizRepository = Mockito.mock(QuizRepository.class);
    private final UserAssembler sut = new UserAssemblerImpl(quizRepository);

    private static final UUID MOCK_ID = UUID.fromString("8ca949a3-e7f7-4a42-a4a3-2880bbbda528");

    @BeforeEach
    public void init() {
        Quiz quiz = getMockQuiz();
        Mockito.when(quizRepository
                .getByIds(Mockito.any()))
                .thenReturn(List.of(quiz));
    }

    private Quiz getMockQuiz() {
        Quiz quiz = new Quiz();
        quiz.setId(MOCK_ID);
        quiz.setName("mockQuiz");
        return quiz;
    }

    @Test
    public void shouldAssembleUserEntityFromUserDto() {
        //GIVEN
        User expected = getMockUser();

        UserDto userDto = new UserDto();
        userDto.setId(expected.getId());
        userDto.setEmail(expected.getEmail());
        userDto.setName(expected.getName());

        QuizResultDto quizResultDto = new QuizResultDto();
        quizResultDto.setId(MOCK_ID);
        quizResultDto.setQuizId(MOCK_ID);
        userDto.setQuizResults(List.of(quizResultDto));

        //WHEN
        User actual = sut.assemble(userDto);

        //THEN

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void shouldAssembleUserDtoFromUserEntity() {
        //GIVEN
        UserDto expected = getMockUserDto();
        User entity = new User();
        entity.setId(expected.getId());
        entity.setName(expected.getName());
        entity.setEmail(expected.getEmail());
        entity.setQuizResults(List.of(getMockQuizResult()));

        //WHEN
        UserDto actual = sut.assemble(entity);

        //THEN
        Assertions.assertEquals(expected, actual);

    }

    private UserDto getMockUserDto() {
        UserDto dto = new UserDto();
        dto.setEmail("email");
        dto.setName("name");

        QuizResult quizResult = getMockQuizResult();
        QuizResultDto quizResultDto = new QuizResultDto();
        quizResultDto.setId(quizResult.getId());
        quizResultDto.setQuizId(quizResult.getQuiz().getId());
        dto.setQuizResults(List.of(quizResultDto));

        return dto;
    }

    private User getMockUser() {
        User user = new User();
        user.setName("name");
        user.setEmail("email");

        QuizResult quizResult = getMockQuizResult();
        quizResult.setUser(user);

        user.setQuizResults(List.of(quizResult));

        return user;
    }

    private QuizResult getMockQuizResult() {
        QuizResult quizResult = new QuizResult();
        quizResult.setId(MOCK_ID);
        Quiz quiz = getMockQuiz();
        quizResult.setQuiz(quiz);
        return quizResult;
    }
}
