package com.my.hibernate.service;

import com.my.hibernate.dto.UserDto;
import com.my.hibernate.entity.User;

public interface UserAssembler {
    User assemble(UserDto dto);

    UserDto assemble(User user);
}
