package com.my.hibernate.service;

import com.my.hibernate.dto.UserDto;
import com.my.hibernate.entity.QuizResult;
import com.my.hibernate.entity.User;
import com.my.hibernate.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserAssembler userAssembler;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserAssembler userAssembler) {
        this.userRepository = userRepository;
        this.userAssembler = userAssembler;
    }

    @Override
    @Transactional
    public UserDto create(UserDto dto) {
        User user = userAssembler.assemble(dto);
        userRepository.save(user);
        return userAssembler.assemble(user);
    }


    @Override
    @Transactional(readOnly = true)
    public UserDto get(String email) {
        User user = userRepository.get(email)
                .orElseThrow(RuntimeException::new);
        return userAssembler.assemble(user);
    }

    @Override
    @Transactional
    public UserDto update(UserDto dto) {
        User persistentUser = userRepository.get(User.class, dto.getId())
                .orElseThrow(RuntimeException::new);
        User updatedUser = userAssembler.assemble(dto);

        performUpdate(persistentUser, updatedUser);

        return userAssembler.assemble(persistentUser);
    }

    private void performUpdate(User persistentUser, User updatedUser) {
        persistentUser.setName(updatedUser.getName());
        persistentUser.setEmail(updatedUser.getEmail());

        updateQuizResults(persistentUser.getQuizResults(), updatedUser.getQuizResults());
    }

    private void updateQuizResults(List<QuizResult> persistentList, List<QuizResult> updatedList) {
        Map<UUID, QuizResult> updatedMap = updatedList
                .stream()
                .filter(q -> Objects.nonNull(q.getId()))
                .collect(Collectors.toMap(QuizResult::getId, Function.identity()));

        Iterator<QuizResult> iterator = persistentList.iterator();
        while (iterator.hasNext()) {
            QuizResult persistentQuizResult = iterator.next();
            if (updatedMap.containsKey(persistentQuizResult.getId())) {
                QuizResult updatedQuizResult = updatedMap.get(persistentQuizResult.getId());
                updateQuizResult(persistentQuizResult, updatedQuizResult);
            } else {
                iterator.remove();
                persistentQuizResult.setUser(null);
            }
        }

        persistentList.addAll(updatedMap.values());
    }

    private void updateQuizResult(QuizResult persistentQuizResult, QuizResult updatedQuizResult) {
        persistentQuizResult.setQuiz(updatedQuizResult.getQuiz());
        persistentQuizResult.setUser(updatedQuizResult.getUser());
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        userRepository.delete(User.class, id);
    }
}
