package com.my.hibernate.service;

import com.my.hibernate.dto.QuizResultDto;
import com.my.hibernate.dto.UserDto;
import com.my.hibernate.entity.Quiz;
import com.my.hibernate.entity.QuizResult;
import com.my.hibernate.entity.User;
import com.my.hibernate.repository.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserAssemblerImpl implements UserAssembler {
    private final QuizRepository quizRepository;

    @Autowired
    public UserAssemblerImpl(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    @Override
    public User assemble(UserDto dto) {
        User entity = new User();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setEmail(dto.getEmail());

        Map<UUID, Quiz> quizzesDyIds = getQuizzes(dto);

        for (QuizResultDto quizResultDto : dto.getQuizResults()) {
            QuizResult quizResult = assemble(quizResultDto);
            quizResult.setUser(entity);

            Quiz quiz = quizzesDyIds.get(quizResultDto.getQuizId());
            quizResult.setQuiz(Objects
                    .requireNonNull(quiz, String.format("Quiz with id: %s does not exist", quizResultDto.getQuizId())));

            entity.getQuizResults().add(quizResult);
        }
        return entity;
    }

    private QuizResult assemble(QuizResultDto dto) {
        QuizResult entity = new QuizResult();
        entity.setId(dto.getId());
        return entity;
    }

    private Map<UUID, Quiz> getQuizzes(UserDto dto) {
        List<UUID> quizzesIds = dto.getQuizResults()
                .stream()
                .map(QuizResultDto::getQuizId)
                .collect(Collectors.toList());

        List<Quiz> quizzes = quizRepository.getByIds(quizzesIds);

        return quizzes
                .stream()
                .collect(Collectors.toMap(Quiz::getId, Function.identity()));
    }

    @Override
    public UserDto assemble(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());

        List<QuizResultDto> quizResultDtos = entity.getQuizResults()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList());

        dto.setQuizResults(quizResultDtos);
        return dto;
    }

    private QuizResultDto assemble(QuizResult entity) {
        QuizResultDto dto = new QuizResultDto();
        dto.setId(entity.getId());
        dto.setQuizId(entity.getQuiz().getId());
        return dto;
    }
}
