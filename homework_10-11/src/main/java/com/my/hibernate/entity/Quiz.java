package com.my.hibernate.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class Quiz {
    @Id
    @Column(length = 16)
    private UUID id = UUID.randomUUID();

    @Column(nullable = false)
    private String name;
}
