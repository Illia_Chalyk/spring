package com.my.hibernate.repository;

import com.my.hibernate.entity.Quiz;

import java.util.List;
import java.util.UUID;

public interface QuizRepository extends GenericRepository<Quiz> {
    List<Quiz> getByIds(List<UUID> ids);

    List<Quiz> getAll();
}
