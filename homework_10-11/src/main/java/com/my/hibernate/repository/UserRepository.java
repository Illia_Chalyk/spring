package com.my.hibernate.repository;

import com.my.hibernate.entity.User;

import java.util.Optional;

public interface UserRepository extends GenericRepository<User> {
    Optional<User> get(String email);
}
