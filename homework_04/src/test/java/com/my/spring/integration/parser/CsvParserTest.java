package com.my.spring.integration.parser;

import com.my.spring.integration.domain.Order;
import com.my.spring.integration.domain.OrderState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CsvParserTest {
    private static Mapper<Order> mapper;
    private static List<Order> expected;

    @BeforeAll
    private static void setup() {
        mapper = arr -> {
            Order order = new Order();
            order.setOrderId(Integer.parseInt(arr[0]));
            order.setOrderState(OrderState.valueOf(arr[1]));
            return order;
        };

        expected = Arrays.asList(
                new Order(1, OrderState.CANCELED),
                new Order(2, OrderState.PAYMENT_COMPLETED),
                new Order(3, OrderState.WAITING_FOR_PAYMENT),
                new Order(4, OrderState.WAITING_FOR_PAYMENT)
        );
    }

    @Test
    public void shouldParseCSV() {
        String csv = "1,CANCELED\r\n" +
                "2,PAYMENT_COMPLETED\r\n" +
                "3,WAITING_FOR_PAYMENT\r\n" +
                "4,WAITING_FOR_PAYMENT\r\n";
        CsvParser parser = new CsvParser();
        List<Order> actual = parser.parse(csv, mapper);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void shouldParseCsvWithCustomDelimiters() {
        String csv = "1;CANCELED\n" +
                "2;PAYMENT_COMPLETED\n" +
                "3;WAITING_FOR_PAYMENT\n" +
                "4;WAITING_FOR_PAYMENT\n";
        CsvParser parser = new CsvParser(";", "\n");
        List<Order> actual = parser.parse(csv, mapper);

        Assertions.assertEquals(expected, actual);
    }
}
