package com.my.spring.integration.parser;

import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class CsvParser {
    private String entryDelimiter;
    private String rowDelimiter;

    public static final String DEFAULT_ENTRY_DELIMITER = ",";
    public static final String DEFAULT_ROW_DELIMITER = "\r\n";

    public CsvParser() {
        this.rowDelimiter = DEFAULT_ROW_DELIMITER;
        this.entryDelimiter = DEFAULT_ENTRY_DELIMITER;
    }

    public CsvParser(String entryDelimiter, String rowDelimiter) {
        this.entryDelimiter = entryDelimiter;
        this.rowDelimiter = rowDelimiter;
    }

    public <T> List<T> parse(String toParse, Mapper<T> mapper) {
        String[] rows = toParse.split(rowDelimiter);
        List<T> list = new ArrayList<>();
        for (String s : rows) {
            list.add(mapper.map(s.split(entryDelimiter)));
        }
        return list;
    }
}
