package com.my.spring.integration;

import com.my.spring.integration.cofigs.AppContextConfig;
import com.my.spring.integration.domain.Order;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.integration.annotation.Gateway;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext();
        context.refresh();
        List<Order> orders = ((AppContextConfig.MyGateway) context.getBean("myGate"))
                .process(new File("./"));
        for (Order o : orders) {
            System.out.println(o);
        }
    }
}
