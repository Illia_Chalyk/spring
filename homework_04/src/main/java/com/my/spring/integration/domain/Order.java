package com.my.spring.integration.domain;

import java.util.Objects;

public class Order {
    private int orderId;
    private OrderState orderState;

    public Order() {
    }

    public Order(int orderId, OrderState orderState) {
        this.orderId = orderId;
        this.orderState = orderState;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId && orderState == order.orderState;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderState);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", orderState=" + orderState +
                '}';
    }
}
