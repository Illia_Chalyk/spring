package com.my.spring.integration.parser;

@FunctionalInterface
public interface Mapper<T> {
    T map(String... args);
}
