package com.my.spring.integration.cofigs;

import com.my.spring.integration.domain.Order;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.file.FileReadingMessageSource;

import java.io.File;
import java.util.List;

@Configuration
@ComponentScan("com.my.spring")
@EnableIntegration
@IntegrationComponentScan
public class AppContextConfig {
    @MessagingGateway(name = "myGate")
    public interface MyGateway {
        @Gateway(replyChannel = "orderFlow.input")
        List<Order> process(File file);
    }

    @Bean
    public FileReadingMessageSource fileSource(File in) {
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(in);
        return source;
    }

    @Bean("myFlow")
    public IntegrationFlow orderFlow(@Value("${input.csv}:./orders.csv") File in) {
        return IntegrationFlows.from(fileSource(in))
                .log()
                .<String[], String>transform(p -> {
                    StringBuilder sb = new StringBuilder();
                    for (String s : p) {
                        sb.append(s);
                    }
                    return sb.toString();
                })
                .get();
//                .handle(message -> new CsvParser()
//                        .<Order>parse(message.getPayload(), (orderId, orderState) -> {
//                            Order order = new Order();
//                            order.setOrderId(orderId);
//                            order.setOrderState(orderState);
//                            return order;
//                        }))
//                .filter()
    }
}
