<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Homepage" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%-- TODO add validation into it --%>
<div>
    <div style="max-width: 400px;margin: auto;" class="text-center mt-5">
        <h1>Testing platform</h1>
    </div>
    <div style="max-width: 400px;height: 600px ;margin: auto;" class="text-center mt-5 bg-light">

        <form style="max-width: 360px;margin: auto;" action="registration" method="post">
            <h1 class="display-6 pt-3 mb-3"><fmt:message key="registration_jsp.registration_form.label"/></h1>

            <div class="row">
                <div class="form-floating mb-3 col">
                    <input class="form-control" name="firstName" type="text" id="floatingInput1"
                           placeholder="Email address"
                           required autofocus>
                    <label for="floatingInput1" style="padding-left: 26px;"><fmt:message
                            key="registration_jsp.registration_form.first_name"/></label>
                </div>
                <div class="form-floating mb-3 col">
                    <input class="form-control" name="lastName" type="text" id="floatingInput2"
                           placeholder="Email address"
                           required>
                    <label for="floatingInput2" style="padding-left: 26px;"><fmt:message
                            key="registration_jsp.registration_form.last_name"/></label>
                </div>
            </div>

            <div class="form-floating mb-3">
                <input class="form-control" name="email" type="email" id="floatingInput3" placeholder="Email address"
                       required>
                <label for="floatingInput3"><fmt:message key="registration_jsp.registration_form.email"/></label>
            </div>

            <div class="form-floating mb-3">
                <input class="form-control" name="password" type="password" id="floatingPassword1"
                       placeholder="Password"
                       required>
                <label for="floatingPassword1"><fmt:message key="registration_jsp.registration_form.password"/></label>
            </div>

            <div class="form-floating mb-4">
                <input class="form-control" name="password2" type="password" id="floatingPassword2"
                       placeholder="Confirm password"
                       required>
                <label for="floatingPassword2"><fmt:message
                        key="registration_jsp.registration_form.password_confirm"/></label>
            </div>

            <div class="form-check text-start mb-4">
                <input class="form-check-input" name="" type="checkbox" value="" id="flexCheckDefault" required>
                <label class="form-check-label" for="flexCheckDefault">
                    <fmt:message key="registration_jsp.registration_form.policies_checkbox.i_accept_the"/> <a
                        href="#"><fmt:message
                        key="registration_jsp.registration_form.policies_checkbox.terms_of_use"/></a> <fmt:message
                        key="registration_jsp.registration_form.policies_checkbox.and"/> <a href="#"><fmt:message
                        key="registration_jsp.registration_form.policies_checkbox.privacy_policy"/></a>
                </label>
            </div>

            <div class="d-grid">
                <input type="submit" class="btn btn-lg btn-dark btn-block" value="Log in"/>
            </div>
        </form>

        <div class="mt-5">
            <a class="link" href="login"><fmt:message key="registration_jsp.anchor.to_login"/></a>
        </div>
    </div>
</div>
</body>
</html>
