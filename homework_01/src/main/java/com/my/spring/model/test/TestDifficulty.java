package com.my.spring.model.test;

public enum TestDifficulty {
    EASY,
    MEDIUM,
    HARD
}
