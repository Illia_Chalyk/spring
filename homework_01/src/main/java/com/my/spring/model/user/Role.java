package com.my.spring.model.user;

public enum Role {
    USER,
    ADMIN;
}
