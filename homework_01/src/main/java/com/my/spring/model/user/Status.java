package com.my.spring.model.user;

public enum Status {
    ACTIVE,
    BLOCKED;
}
