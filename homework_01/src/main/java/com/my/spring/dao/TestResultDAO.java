package com.my.spring.dao;

import com.my.spring.model.test.QuestionAnswerPair;
import com.my.spring.model.test.TestResult;
import com.my.spring.model.user.User;

import java.util.List;
import java.util.Set;

//TODO rename
public interface TestResultDAO {
    List<TestResult> getUserTestResults(User user) throws DAOException;

    void insertTestResult(TestResult testResult) throws DAOException;

    void insertUserAnswers(int attemptId, Set<QuestionAnswerPair> pairSet) throws DAOException;
}
