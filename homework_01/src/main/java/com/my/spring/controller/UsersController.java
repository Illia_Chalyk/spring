package com.my.spring.controller;

import com.my.spring.model.user.User;
import com.my.spring.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class UsersController {
    @Autowired
    UsersService usersService;

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @PostMapping("/login")
    public String login(@RequestParam String email,
                        @RequestParam String password,
                        HttpSession session) {
        if (!usersService.isValidUser(email, password)) {
            return "login";
        }
        usersService.loginUser(session, email);
        return "redirect:/homepage";
    }

    @PostMapping("/logout")
    public String logout(HttpSession session) {
        usersService.logoutUser(session);
        return "redirect:/login";
    }

    @GetMapping("/registration")
    public String getRegistrationPage(HttpSession session) {
        if (session.getAttribute("user") != null) {
            return "homepage";
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String register(@RequestParam String firstName,
                           @RequestParam String lastName,
                           @RequestParam String email,
                           @RequestParam String password,
                           HttpSession session) {
        usersService.registerNewUser(firstName, lastName, email, password);
        usersService.loginUser(session, email);
        return "redirect:/homepage";
    }

    @GetMapping("/profile")
    public String getProfilePage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        usersService.getUserTestResults(model, session);
        return "profile";
    }
}
