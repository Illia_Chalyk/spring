package com.my.spring.service;

import org.springframework.ui.Model;

public interface TestsService {
    void getTests(Model model, String currentPage);

    void getTest(int testId, Model model);

    int addNewTest(String testName,
                   String subject,
                   String difficulty,
                   int timeLimitInMinutes,
                   String description);

    void deleteTest(int testId);

    void updateTest(int testId,
                    String testName,
                    String subject,
                    String difficulty,
                    int timeLimitInMinutes,
                    String description,
                    String submit);

    void addQuestionToTest(String questionText, int testId);

    void editQuestion(int questionId, String questionText);

    void deleteQuestion(int questionId);

    void abbAnswerToQuestion(String answerText, boolean correct, int questionId);

    void editAnswer(int answerId, boolean correct, String answerText);

    void deleteAnswer(int answerId);
}
