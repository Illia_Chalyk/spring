package com.my.spring;

import com.my.spring.service.CalcService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("context.xml");
        CalcService calc = context.getBean(CalcService.class);
        System.out.println(calc.add(7, 3, 5, 18));
    }
}
