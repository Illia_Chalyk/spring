package com.my.spring.service;

import com.my.spring.annotation.Logger;

import java.util.Arrays;

public class CalcService {
    @Logger
    public int add(int x, int y, int... other) {
        return x + y + Arrays.stream(other).sum();
    }
}
