package com.my.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerAspect {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.my.spring.annotation.Logger)")
    public void logger() {
    }

    @Around("logger()")
    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        try {
            log.info("Start executing {}", methodName);
            Object returnValue = joinPoint.proceed();
            log.info("{} successfully executed", methodName);
            return returnValue;
        } catch (Throwable t) {
            log.error(String.format("Execution of %s caused an Exception", methodName), t);
            throw new RuntimeException(t);
        }
    }
}
