package com.my.spring.service;

import com.my.spring.annotation.Logger;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class CalcService {
    @Logger
    public int add(int x, int y, int... other) {
        return x + y + Arrays.stream(other).sum();
    }
}
