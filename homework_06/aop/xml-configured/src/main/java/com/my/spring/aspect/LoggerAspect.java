package com.my.spring.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerAspect {
    private Logger log = LoggerFactory.getLogger(getClass());

    public Object aroundAdvice(ProceedingJoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        try {
            log.info("Start executing {}", methodName);
            Object returnValue = joinPoint.proceed();
            log.info("{} successfully executed", methodName);
            return returnValue;
        } catch (Throwable t) {
            log.error(String.format("Execution of %s caused an Exception", methodName), t);
            throw new RuntimeException(t);
        }
    }
}
