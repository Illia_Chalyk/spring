DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL
);

INSERT INTO users (first_name, last_name) VALUES
    ('Adrian', 'Langosh'),
    ('Holden', 'Kuhic'),
    ('Veda', 'Balistreri'),
    ('Richmond', 'Lowe'),
    ('Stuart', 'Nicolas'),
    ('Fausto', 'Crona'),
    ('Jayda', 'Brakus'),
    ('Cleora', 'OHara'),
    ('Dayana', 'Becker'),
    ('Kiana', 'Sporer'),
    ('Margaret', 'Cassin'),
    ('Lillian', 'Reinger'),
    ('Filomena', 'Dibbert'),
    ('Lora', 'Bogan'),
    ('Israel', 'Kunde'),
    ('Willard', 'Gorczany'),
    ('Elnora', 'Kiehn'),
    ('Tanya', 'Heaney'),
    ('Columbus', 'Blick'),
    ('Ida', 'Eichmann'),
    ('Gerry', 'Ruecker'),
    ('Mathew', 'Rohan'),
    ('Louie', 'McLaughlin'),
    ('Noemi', 'Huels'),
    ('Linwood', 'Haley'),
    ('Stacey', 'Heller'),
    ('Melisa', 'Wyman'),
    ('Annabell', 'Jones'),
    ('Claire', 'Schinner'),
    ('Walter', 'Ryan'),
    ('Annalise', 'Corwin'),
    ('Melba', 'Flatley'),
    ('Hollis', 'White'),
    ('Lilliana', 'Baumbach'),
    ('Jamarcus', 'Brown'),
    ('Reynold', 'Kunde'),
    ('Kara', 'Jerde'),
    ('Reanna', 'Hintz'),
    ('Imani', 'Bogisich'),
    ('Adolf', 'Jaskolski'),
    ('Garth', 'Wintheiser'),
    ('Lilliana', 'Steuber'),
    ('Jody', 'Renner'),
    ('Annetta', 'Schinner'),
    ('Josefa', 'Glover'),
    ('Twila', 'Kirlin'),
    ('Gay', 'Ledner'),
    ('Jess', 'Kohler'),
    ('Jaleel', 'Dibbert'),
    ('Chaya', 'Dietrich'),
    ('Nakia', 'Bode'),
    ('Davon', 'Dickens'),
    ('Pearl', 'Harris'),
    ('Enid', 'Pagac'),
    ('Margarita', 'Turner'),
    ('Ryleigh', 'Swaniawski'),
    ('Bell', 'Veum'),
    ('Leonard', 'Kautzer'),
    ('Muriel', 'Gulgowski'),
    ('Micaela', 'Mills'),
    ('Wyman', 'Lind'),
    ('Jacky', 'Barrows'),
    ('Letha', 'Pacocha'),
    ('Garrison', 'Kozey'),
    ('Dino', 'Ryan'),
    ('Gillian', 'Lubowitz'),
    ('Collin', 'Glover'),
    ('Mertie', 'Fisher'),
    ('Chelsey', 'Tremblay'),
    ('Joyce', 'Halvorson'),
    ('Gilbert', 'Witting'),
    ('Alene', 'Heathcote'),
    ('Janis', 'Kirlin'),
    ('Aida', 'Nolan'),
    ('Stevie', 'Kuhn'),
    ('Kiarra', 'Jerde'),
    ('Tiana', 'Will'),
    ('Obie', 'Grant'),
    ('Elyssa', 'Schneider'),
    ('Francis', 'Zulauf'),
    ('Alexander', 'McClure'),
    ('Megane', 'Thiel'),
    ('Elise', 'Orn'),
    ('Lulu', 'Blick'),
    ('Seamus', 'Fisher'),
    ('Astrid', 'Fay'),
    ('Liam', 'Jerde'),
    ('Trinity', 'Hand'),
    ('Hannah', 'Mohr'),
    ('Anahi', 'Hermiston'),
    ('Woodrow', 'Klein'),
    ('Reginald', 'Schulist'),
    ('Claudine', 'DAmore'),
    ('Carlotta', 'Windler'),
    ('Darien', 'Pouros'),
    ('Keara', 'Gulgowski'),
    ('Katlyn', 'Schmitt'),
    ('Peyton', 'Legros'),
    ('Esther', 'Durgan');
