package com.epam.springcloud.notification;

import lombok.Data;

@Data
public class UserDto {
    private String name;
}