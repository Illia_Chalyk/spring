package com.epam.springcloud.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.Set;

@RestController
public class NotificationController {
    private final DiscoveryClient discoveryClient;
    private final RestTemplate restTemplate = new RestTemplate();
    private final Set<Notification> notifications = new HashSet<>();

    @Autowired
    public NotificationController(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @PostMapping
    public ResponseEntity<Notification> notify(@RequestParam String user) {
        ServiceInstance userInfo = discoveryClient.getInstances("users").get(0);
        String hostName = userInfo.getHost();
        int port = userInfo.getPort();

        try {
            restTemplate
                    .getForObject("http://" + hostName + ":" + port + "/" + user, UserDto.class);
        } catch (RestClientException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Notification notification = new Notification();
        notification.setUser(user);
        notifications.add(notification);

        return new ResponseEntity<>(notification, HttpStatus.OK);
    }

    @GetMapping
    public Set<Notification> getNotifications() {
        return notifications;
    }
}
