package com.pigorv.springcloud.orders;

import com.pigorv.springcloud.orders.feign.NotificationClient;
import com.pigorv.springcloud.orders.feign.ProductClient;
import com.pigorv.springcloud.orders.feign.UserClient;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RequestMapping
@RestController
public class OrdersController {
    private List<Order> orderList = new ArrayList<>();

    UserClient userClient;
    ProductClient productClient;
    NotificationClient notificationClient;

    @Autowired
    public OrdersController(UserClient userClient,
                            ProductClient productClient,
                            NotificationClient notificationClient) {
        this.userClient = userClient;
        this.productClient = productClient;
        this.notificationClient = notificationClient;
    }

    @GetMapping
    public String health() {
        return "OK";
    }

    @PostMapping
    public ResponseEntity<Order> createNewOrder(@RequestBody Order order) {
        try {
            userClient.getUser(order.getUserName());
            productClient.removeOneProduct(order.getProduct());
        } catch (FeignException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        notificationClient.notify(order.getUserName());

        orderList.add(order);

        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @GetMapping("/users/{userName}")
    public List<String> getProductsForUser(@PathVariable String userName) {
        return orderList.stream()
                .filter(order -> userName.equals(order.getUserName()))
                .map(Order::getProduct)
                .collect(toList());
    }

    @Bean
    private RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
