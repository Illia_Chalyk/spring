package com.pigorv.springcloud.orders.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "notifications")
public interface NotificationClient {
    @PostMapping
    void notify(@RequestParam String user);
}
