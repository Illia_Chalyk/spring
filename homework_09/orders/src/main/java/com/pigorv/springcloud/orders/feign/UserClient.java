package com.pigorv.springcloud.orders.feign;

import com.pigorv.springcloud.orders.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "users")
public interface UserClient {
    @GetMapping("/{userName}")
    UserDto getUser(@PathVariable String userName);
}
