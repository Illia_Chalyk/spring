<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Error page" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div align="center">
    <label><b>An error occurred while processing your request.</b></label>
    <a href="homepage">Go to homepage</a>
</div>
</body>
</html>
