<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Tests info" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div align="center">
    <h3><c:out value="${test.name}"/></h3>
    <span>Subject: </span><c:out value="${test.subject}"/><br>
    <span>Test difficulty: </span><c:out value="${test.difficulty}"/><br>
    <span>Number of questions: </span><c:out value="${test.numberOfQuestions}"/><br>
    <span>Time limit in minutes: </span><c:out value="${test.timeLimitInMinutes}"/><br>
    <span>Description: </span><c:out value="${test.description}"/><br>
    <c:choose>
        <c:when test="${test.status.toString().equals('SUBMITTED')}">
            <br>
            <a href="passing?testId=${test.id}">Pass the test</a>
            <br>
        </c:when>
        <c:when test="${test.status.toString().equals('EDITING') && user.role.toString().equals('ADMIN')}">
            <br>
            <a href="testedit?testId=${test.id}">Edit test</a>
            <br>
        </c:when>
    </c:choose>
</div>
</body>
</html>
