<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="Add a new test" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div align="center">
    <form action="addtest" method="post">
        <input type="text" name="testName" placeholder="Test name" required><br>
        <input type="text" name="subject" placeholder="Subject" required><br>
        <select name="difficulty" required><br>
            <option disabled selected>Test difficulty</option>
            <option>Easy</option>
            <option>Medium</option>
            <option>Hard</option>
        </select><br>
        <input type="number" min="1" max="300" name="timeLimitInMinutes" placeholder="Time limit in minutes" required><br>
        <textarea type="text" name="description" placeholder="Description" required></textarea><br>
        <input type="submit" value="Submit">
    </form>
</div>
</body>
</html>
