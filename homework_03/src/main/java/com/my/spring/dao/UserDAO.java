package com.my.spring.dao;

import com.my.spring.model.user.User;

public interface UserDAO {
    void insertUser(User user) throws DAOException;

    User getUserByEmail(String email) throws DAOException;

    boolean isUserExist(User user) throws DAOException;
}
