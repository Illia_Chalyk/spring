package com.my.spring.model.test;

public enum TestStatus {
    EDITING,
    SUBMITTED,
    ARCHIVED
}
