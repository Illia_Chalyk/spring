package com.my.spring.model.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Question {
    private int id;
    private String questionText;
    //TODO refactor logic with files
    private File imgFile;
    private List<Answer> answers;
    public int getId() {
        return id;
    }

    public Question() {
        answers = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public File getImgFile() {
        return imgFile;
    }

    public void setImgFile(File imgFile) {
        this.imgFile = imgFile;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", questionText='" + questionText + '\'' +
                ", imgFile=" + imgFile +
                ", answers=" + answers +
                '}';
    }
}
