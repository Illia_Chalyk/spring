package com.my.spring.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

public class LocaleFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession();
        String localeToSet = req.getParameter("locale");
        if (localeToSet != null && session != null) {
            session.setAttribute("locale", new Locale(localeToSet));
        }
        //TODO exception handling
        chain.doFilter(req, res);
    }
}
