package com.my.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@ControllerAdvice
public class GlobalControllerExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public String defaultExceptionHandler(HttpServletRequest req, Exception e) throws Exception {
        log.error("Request: {}, raised:", req.getRequestURL(), e);
        Object annotation = AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class);
        if (Objects.nonNull(annotation)) {
            throw e;
        }
        return "error";
    }
}
