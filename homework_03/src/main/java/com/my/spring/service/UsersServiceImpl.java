package com.my.spring.service;

import com.my.spring.bpps.Timed;
import com.my.spring.dao.DAOException;
import com.my.spring.dao.TestResultDAO;
import com.my.spring.dao.UserDAO;
import com.my.spring.model.test.TestResult;
import com.my.spring.model.user.User;
import com.my.spring.util.PasswordProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

@Timed
@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    UserDAO userDAO;
    @Autowired
    private TestResultDAO testResultDAO;
    @Autowired
    PasswordProcessor passwordProcessor;

    @Override
    public boolean isValidUser(String email, String password) {
        boolean flag = false;
        try {
            User user = userDAO.getUserByEmail(email);
            if (Objects.nonNull(user)) {
                flag = passwordProcessor
                        .comparePasswords(password, user.getPassword(), user.getPasswordSalt());
            }
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public void loginUser(HttpSession session, String email) {
        User user = null;
        try {
            user = userDAO.getUserByEmail(email);
            session.setAttribute("isLogged", true);
            session.setAttribute("user", user);
            //TODO locale setting
            //session.setAttribute("locale", req.getLocale());
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void registerNewUser(String firstName,
                                String lastName,
                                String email,
                                String password) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);

        String[] passAndSalt = passwordProcessor.hashPassword(password);
        String hashedPassword = passAndSalt[0];
        String passwordSalt = passAndSalt[1];
        user.setPassword(hashedPassword);
        user.setPasswordSalt(passwordSalt);
        try {
            userDAO.isUserExist(user);
            userDAO.insertUser(user);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void logoutUser(HttpSession session) {
        session.invalidate();
    }

    @Override
    public void getUserTestResults(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        try {
            List<TestResult> results = testResultDAO.getUserTestResults(user);
            model.addAttribute("results", results);
        } catch (DAOException e) {
            //TODO exception handling;
            e.printStackTrace();
        }
    }
}
