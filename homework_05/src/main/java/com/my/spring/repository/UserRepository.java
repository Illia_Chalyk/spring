package com.my.spring.repository;


import com.my.spring.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    @Override
    Page<User> findAll(Pageable pageable);

    @Override
    Optional<User> findById(Integer integer);
}
