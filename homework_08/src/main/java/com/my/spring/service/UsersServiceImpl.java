package com.my.spring.service;

import com.my.spring.dao.DAOException;
import com.my.spring.dao.TestResultDAO;
import com.my.spring.dao.UserDAO;
import com.my.spring.model.test.TestResult;
import com.my.spring.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    private UserDAO userDAO;
    private TestResultDAO testResultDAO;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UsersServiceImpl(UserDAO userDAO, TestResultDAO testResultDAO, PasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.testResultDAO = testResultDAO;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerNewUser(String firstName,
                                String lastName,
                                String email,
                                String password) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(password));
        try {
            userDAO.isUserExist(user);
            userDAO.insertUser(user);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void getUserTestResults(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        try {
            List<TestResult> results = testResultDAO.getUserTestResults(user);
            model.addAttribute("results", results);
        } catch (DAOException e) {
            //TODO exception handling;
            e.printStackTrace();
        }
    }
}
