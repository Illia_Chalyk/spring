package com.my.spring.service;

import com.my.spring.dao.TestDAO;
import com.my.spring.dto.TestAddDTO;
import com.my.spring.dto.TestUpdateDTO;
import com.my.spring.model.test.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class TestsServiceImpl implements TestsService {
    private TestDAO testDAO;

    @Autowired
    public TestsServiceImpl(TestDAO testDAO) {
        this.testDAO = testDAO;
    }

    @Override
    public List<Test> getTests(int currentPage) {
        int recordsPerPage = 10;
        List<Test> tests = testDAO.getAllTests(recordsPerPage,
                currentPage * recordsPerPage - recordsPerPage);
        return tests;
    }

    @Override
    public int getNumberOfTests() {
        return testDAO.getNumberOfTests();
    }

    @Override
    public Test getTest(int testId) {
        Test test = testDAO.getTestById(testId);
        return test;
    }

    @Override
    public int addNewTest(TestAddDTO dto) {
        Test test = new Test();
        test.setName(dto.getTestName());
        test.setSubject(dto.getSubject());
        test.setDifficulty(TestDifficulty.valueOf(dto.getDifficulty().toUpperCase()));
        test.setTimeLimitInMinutes(dto.getTimeLimitInMinutes());
        test.setDescription(dto.getDescription());
        testDAO.insertTest(test);
        return test.getId();
    }

    @Override
    public void deleteTest(int testId) {
        testDAO.deleteTestDyId(testId);
    }

    @Override
    public void updateTest(TestUpdateDTO dto) {
        Test test = testDAO.getTestById(dto.getTestId());
        if (Objects.nonNull(dto.getSubmit())) {
            test.setStatus(TestStatus.SUBMITTED);
        } else {
            test.setName(dto.getTestName());
            test.setSubject(dto.getSubject());
            test.setDifficulty(TestDifficulty.valueOf(dto.getDifficulty().toUpperCase()));
            test.setTimeLimitInMinutes(dto.getTimeLimitInMinutes());
            test.setDescription(dto.getDescription());
        }
        testDAO.updateTest(test);
    }

    @Override
    public void addQuestionToTest(String questionText, int testId) {
        Question question = new Question();
        question.setQuestionText(questionText);
        Test test = new Test();
        test.setId(testId);
        testDAO.addQuestionToTest(test, question);
    }

    @Override
    public void editQuestion(int questionId, String questionText) {
        Question question = testDAO.getQuestionById(questionId);
        question.setQuestionText(questionText);
        testDAO.updateQuestion(question);
    }

    @Override
    public void deleteQuestion(int questionId) {
        testDAO.deleteQuestionById(questionId);
    }

    @Override
    public void abbAnswerToQuestion(String answerText,
                                    boolean correct,
                                    int questionId) {
        Answer answer = new Answer();
        answer.setAnswerText(answerText);
        answer.setCorrect(correct);
        Question question = testDAO.getQuestionById(questionId);
        testDAO.addAnswerToQuestion(question, answer);
    }

    @Override
    public void editAnswer(int answerId, boolean correct, String answerText) {
        Answer answer = testDAO.getAnswerById(answerId);
        answer.setAnswerText(answerText);
        answer.setCorrect(correct);
        testDAO.updateAnswer(answer);
    }

    @Override
    public void deleteAnswer(int answerId) {
        testDAO.deleteAnswerById(answerId);
    }
}
