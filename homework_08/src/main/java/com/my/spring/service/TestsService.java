package com.my.spring.service;

import com.my.spring.dto.TestAddDTO;
import com.my.spring.dto.TestUpdateDTO;
import com.my.spring.model.test.Test;

import java.util.List;

public interface TestsService {
    List<Test> getTests(int currentPage);

    int getNumberOfTests();

    Test getTest(int testId);

    int addNewTest(TestAddDTO dto);

    void deleteTest(int testId);

    void updateTest(TestUpdateDTO dto);

    void addQuestionToTest(String questionText, int testId);

    void editQuestion(int questionId, String questionText);

    void deleteQuestion(int questionId);

    void abbAnswerToQuestion(String answerText, boolean correct, int questionId);

    void editAnswer(int answerId, boolean correct, String answerText);

    void deleteAnswer(int answerId);
}
