package com.my.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.jndi.JndiTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
    @Bean
    @Profile("default")
    public DataSource jndiDataSource() throws Exception {
        JndiTemplate jndiTemplate = new JndiTemplate();
        DataSource dataSource = jndiTemplate
                .lookup("java:/comp/env/jdbc/testing", DataSource.class);
        return dataSource;
    }

    @Bean
    @Profile("test")
    public DataSource h2DataSource() {
        EmbeddedDatabaseBuilder databaseBuilder = new EmbeddedDatabaseBuilder();
        DataSource dataSource = databaseBuilder
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .build();
        return dataSource;
    }
}
