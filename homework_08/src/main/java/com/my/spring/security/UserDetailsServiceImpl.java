package com.my.spring.security;

import com.my.spring.dao.UserDAO;
import com.my.spring.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        User user = userDAO.getUserByEmail(email);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException("Incorrect credentials");
        }
        return SecurityUser.fromUser(user);
    }
}
