package com.my.spring.dto;

import lombok.Data;

@Data
public class TestUpdateDTO {
    int testId;
    String testName;
    String subject;
    String difficulty;
    int timeLimitInMinutes;
    String description;
    String submit;
}
