package com.my.spring.dto;

import lombok.Data;

@Data
public class TestAddDTO {
    String testName;
    String subject;
    String difficulty;
    int timeLimitInMinutes;
    String description;
}
