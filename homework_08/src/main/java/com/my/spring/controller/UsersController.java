package com.my.spring.controller;

import com.my.spring.model.user.User;
import com.my.spring.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class UsersController {
    private UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/registration")
    public String getRegistrationPage(HttpSession session) {
        if (session.getAttribute("user") != null) {
            return "homepage";
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String register(@RequestParam String firstName,
                           @RequestParam String lastName,
                           @RequestParam String email,
                           @RequestParam String password,
                           HttpSession session) {
        usersService.registerNewUser(firstName, lastName, email, password);
        return "redirect:/homepage";
    }

    @GetMapping("/profile")
    public String getProfilePage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        usersService.getUserTestResults(model, session);
        return "profile";
    }
}
