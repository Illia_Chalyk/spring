package com.my.spring.controller;

import com.my.spring.dto.TestAddDTO;
import com.my.spring.dto.TestUpdateDTO;
import com.my.spring.model.test.Test;
import com.my.spring.service.TestsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Objects;

@Controller
public class TestsController {
    private TestsService testsService;

    @Autowired
    public TestsController(TestsService testsService) {
        this.testsService = testsService;
    }

    @GetMapping("/tests")
    public String getTestsPage(@RequestParam(required = false) Integer currentPage, Model model) {
        if (Objects.isNull(currentPage)) {
            currentPage = 1;
        }
        List<Test> tests = testsService.getTests(currentPage);
        model.addAttribute("tests", tests);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("recordsPerPage", 10);
        model.addAttribute("numOfPages", testsService.getNumberOfTests());
        return "tests";
    }


    @GetMapping("/info")
    public String getTestInfo(@RequestParam int testId, Model model) {
        Test test = testsService.getTest(testId);
        model.addAttribute("test", test);
        return "testinfo";
    }

    @GetMapping("/addtest")
    public String getAddTestPage() {
        return "addtest";
    }

    @PostMapping("/addtest")
    public String addNewTest(@RequestBody TestAddDTO dto) {
        int id = testsService.addNewTest(dto);
        return "redirect:/testedit?testId=" + id;
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping("/deletetest")
    public String deleteTest(@RequestParam int testId) {
        testsService.deleteTest(testId);
        return "redirect:/tests";
    }

    @GetMapping("/testedit")
    public String getTestEditPage(@RequestParam int testId, Model model) {
        Test test = testsService.getTest(testId);
        model.addAttribute("test", test);
        return "testedit";
    }

    //HTML does not support HTTP PATCH METHOD.
    @PostMapping("/testedit")
    public String updateTest(@RequestBody TestUpdateDTO dto, Model model) {
        testsService.updateTest(dto);
        model.addAttribute("testId", dto.getTestId());
        return "redirect:/testedit";
    }

    @PostMapping("/addquestion")
    public String addQuestion(@RequestParam String questionText,
                              @RequestParam int testId,
                              Model model) {
        testsService.addQuestionToTest(questionText, testId);

        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/editquestion")
    public String editQuestion(@RequestParam int questionId,
                               @RequestParam String questionText,
                               @RequestParam int testId,
                               Model model) {
        testsService.editQuestion(questionId, questionText);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping("/deletequestion")
    public String deleteQuestion(@RequestParam int questionId,
                                 @RequestParam int testId,
                                 Model model) {
        testsService.deleteQuestion(questionId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/addanswer")
    public String addAnswer(@RequestParam String answerText,
                            @RequestParam boolean correct,
                            @RequestParam int questionId,
                            @RequestParam int testId,
                            Model model) {
        testsService.abbAnswerToQuestion(answerText, correct, questionId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    @PostMapping("/editanswer")
    public String editAnswer(@RequestParam int answerId,
                             @RequestParam boolean correct,
                             @RequestParam String answerText,
                             @RequestParam int testId,
                             Model model) {
        testsService.editAnswer(answerId, correct, answerText);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }

    //HTML does not support HTTP DELETE METHOD.
    @PostMapping
    public String deleteAnswer(@RequestParam int answerId,
                               @RequestParam int testId,
                               Model model) {
        testsService.deleteAnswer(answerId);
        model.addAttribute("testId", testId);
        return "redirect:/testedit";
    }


}
