package com.my.spring.dao.rdb;

import com.my.spring.dao.DAOFactory;
import com.my.spring.dao.TestDAO;
import com.my.spring.dao.TestResultDAO;
import com.my.spring.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Component
public class RdbDAOFactory implements DAOFactory {
    @Autowired
    private DataSource dataSource;

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public static void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                //TODO exception handling
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserDAO getUserDAO() {
        return new RdbUserDAO();
    }

    @Override
    public TestDAO getTestDAO() {
        return new RdbTestDAO();
    }

    @Override
    public TestResultDAO getTestResultDAO() {
        return new RdbTestResultDAO();
    }
}
