package com.my.spring.model.test;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Test {
    private int id;
    private String name;
    private String subject;
    private TestDifficulty difficulty;
    private int numberOfQuestions;
    private int timeLimitInMinutes;
    private String description;
    private TestStatus status;
    private List<Question> questions;

    public Test() {
        questions = new ArrayList<>();
    }
}

