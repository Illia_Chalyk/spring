package com.my.spring.controller;

import com.my.spring.model.test.TestDifficulty;
import com.my.spring.model.test.TestStatus;
import com.my.spring.service.TestsService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


public class TestControllerTest {
    private final TestsService testsService = Mockito.mock(TestsService.class);
    private final TestsController sut = new TestsController(testsService);
    private final MockMvc mockMvc = standaloneSetup(sut).setViewResolvers(internalResourceViewResolver()).build();

    private ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Test
    public void shouldReturnHtmlPageWithAllTests() throws Exception {
        //GIVEN
        List<com.my.spring.model.test.Test> testList = Arrays.asList(
                new com.my.spring.model.test.Test(
                        1, "name", "subject",
                        TestDifficulty.EASY, 1,
                        1, "description",
                        TestStatus.SUBMITTED, new ArrayList<>()),
                new com.my.spring.model.test.Test(
                        1, "name", "subject",
                        TestDifficulty.EASY, 1,
                        1, "description",
                        TestStatus.SUBMITTED, new ArrayList<>()),
                new com.my.spring.model.test.Test(
                        1, "name", "subject",
                        TestDifficulty.EASY, 1,
                        1, "description",
                        TestStatus.SUBMITTED, new ArrayList<>())
        );
        when(testsService.getTests(1)).thenReturn(testList);
        when(testsService.getNumberOfTests()).thenReturn(3);
        //WHEN
        //THEN
        mockMvc.perform(get("/tests")).andDo(print())
                .andExpect(view().name("tests"));
    }
}
