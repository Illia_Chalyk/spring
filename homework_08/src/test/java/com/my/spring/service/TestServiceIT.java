package com.my.spring.service;

import com.my.spring.configuration.AppContextConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

@ActiveProfiles("test")
@SpringJUnitWebConfig(AppContextConfig.class)
public class TestServiceIT {
    @Autowired
    private UsersService sut;

    @Test
    public void shouldRegisterNewUser() {
        Assertions.assertDoesNotThrow(() ->
                sut.registerNewUser("name", "lastname", "email", "password"));
    }
}
