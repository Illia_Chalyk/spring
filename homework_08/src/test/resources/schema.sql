--CREATE TABLE user_role (
--  id tinyint NOT NULL AUTO_INCREMENT,
--  name varchar NOT NULL,
--  PRIMARY KEY (id)
--);
--
--CREATE TABLE user_status (
--  id tinyint NOT NULL AUTO_INCREMENT,
--  name varchar NOT NULL,
--  PRIMARY KEY (id),
--  UNIQUE KEY name_UNIQUE (name)
--);
--
--CREATE TABLE user (
--  id int NOT NULL AUTO_INCREMENT,
--  first_name varchar NOT NULL,
--  last_name varchar NOT NULL,
--  email varchar NOT NULL,
--  password varchar NOT NULL,
--  password_salt varchar NOT NULL,
--  role tinyint NOT NULL DEFAULT '1',
--  status tinyint NOT NULL DEFAULT '1',
--  registration_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
--  PRIMARY KEY (id),
--  UNIQUE KEY email_UNIQUE (email),
--  KEY status_idx (status),
--  KEY role_idx (role),
--  CONSTRAINT role FOREIGN KEY (role) REFERENCES user_role (id) ON DELETE RESTRICT ON UPDATE CASCADE,
--  CONSTRAINT status FOREIGN KEY (status) REFERENCES user_status (id) ON UPDATE CASCADE
--);
CREATE TABLE user_role (
  id tinyint NOT NULL ,
  name varchar NOT NULL
);

CREATE TABLE user_status (
  id tinyint NOT NULL,
  name varchar NOT NULL
);

CREATE TABLE user (
  id int NOT NULL,
  first_name varchar NOT NULL,
  last_name varchar NOT NULL,
  email varchar NOT NULL,
  password varchar NOT NULL,
  password_salt varchar NULL,
  role tinyint NOT NULL DEFAULT '1',
  status tinyint NOT NULL DEFAULT '1',
  registration_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE user_role ALTER id tinyint NOT NULL AUTO_INCREMENT; --ALTER = MODIFY
ALTER TABLE user_role ADD PRIMARY KEY (id);
ALTER TABLE user_status ALTER id tinyint NOT NULL AUTO_INCREMENT; --ALTER = MODIFY
ALTER TABLE user_status ADD PRIMARY KEY (id);
ALTER TABLE user_status ADD UNIQUE KEY user_status_name_UNIQUE (name);
ALTER TABLE user ALTER id int NOT NULL AUTO_INCREMENT; --ALTER = MODIFY
ALTER TABLE user ADD PRIMARY KEY (id);
ALTER TABLE user ADD UNIQUE KEY user_mail_UNIQUE (email);
ALTER TABLE user ADD FOREIGN KEY (role) REFERENCES user_role (id) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE user ADD FOREIGN KEY (status) REFERENCES user_status (id) ON UPDATE CASCADE;