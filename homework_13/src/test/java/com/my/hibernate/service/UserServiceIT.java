package com.my.hibernate.service;

import com.my.hibernate.cfg.HibernateConfiguration;
import com.my.hibernate.dto.QuizResultDto;
import com.my.hibernate.dto.UserDto;
import com.my.hibernate.entity.Quiz;
import com.my.hibernate.entity.User;
import com.my.hibernate.repository.QuizRepository;
import net.bytebuddy.utility.RandomString;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@SpringJUnitConfig(HibernateConfiguration.class)
public class UserServiceIT {
    @Autowired
    private UserService sut;
    @Autowired
    private QuizRepository quizRepository;

    private static final String[] QUIZ_NAMES = {"Math", "Physics", "History", "Art"};
    private Map<String, Quiz> quizzes = Map.of();

    @BeforeEach
    public void init() {
        List<Quiz> initialList = quizRepository.getAll();

        if (initialList.isEmpty()) {
            initialList = createQuizzes();
        }

        quizzes = initialList.stream()
                .collect(Collectors.toMap(Quiz::getName, Function.identity()));
    }

    @Test
    @Transactional
    public void shouldCreateApplicationContext(ApplicationContext context) {
        Assertions.assertNotNull(context);
    }

    @Test
    @Transactional
    public void shouldCreateNewUser() {
        //GIVEN
        UserDto expected = createUserDto();

        //WHEN
        UserDto actual = sut.create(expected);

        //THEN
        Assertions.assertEquals(expected, actual);
        Assertions.assertEquals(expected.getQuizResults().size(), actual.getQuizResults().size());
    }

    @Test
    @Transactional
    public void shouldReadExistingUser() {
        //GIVEN
        UserDto expected = createUserDto();
        sut.create(expected);

        //WHEN
        UserDto actual = sut.get(expected.getEmail());

        //THEN
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @Transactional
    public void shouldUpdateExistingUser() {
        //GIVEN
        UserDto old = createUserDto();
        sut.create(old);
        UserDto updated = createUserDto();
        updated.setId(old.getId());
        updated.setQuizResults(old.getQuizResults());

        //WHEN
        sut.update(updated);

        //THEN
        Assertions.assertEquals(old.getId(), updated.getId());
        Assertions.assertEquals(old.getQuizResults(), updated.getQuizResults());
        Assertions.assertNotEquals(old.getEmail(), updated.getEmail());
        Assertions.assertNotEquals(old.getName(), updated.getName());
    }

    @Test
    @Transactional
    public void shouldDeleteExistingUser() {
        //GIVEN
        UserDto user = createUserDto();
        sut.create(user);

        //WHEN
        sut.delete(user.getId());

        //THEN
        Assertions.assertThrows(RuntimeException.class, () -> sut.get(user.getEmail()));
    }

    private List<Quiz> createQuizzes() {
        return Stream.of(QUIZ_NAMES)
                .map(this::createQuiz)
                .peek(quiz -> quizRepository.save(quiz))
                .collect(Collectors.toList());
    }

    private Quiz createQuiz(String name) {
        Quiz quiz = new Quiz();
        quiz.setName(name);
        return quiz;
    }

    private UserDto createUserDto() {
        UserDto dto = new UserDto();
        dto.setEmail(RandomString.make(8));
        dto.setName(RandomString.make(8));

        QuizResultDto firstQuizResult = new QuizResultDto();
        firstQuizResult.setQuizId(getQuizId(0));
        dto.getQuizResults().add(firstQuizResult);

        QuizResultDto secondQuizResult = new QuizResultDto();
        secondQuizResult.setQuizId(getQuizId(1));
        dto.getQuizResults().add(secondQuizResult);

        return dto;
    }

    private UUID getQuizId(int quizIndex) {
        return quizzes.get(QUIZ_NAMES[quizIndex]).getId();
    }
}
