package com.my.hibernate.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class QuizResultDto {
    private UUID id = UUID.randomUUID();
    private UUID quizId;
}
