package com.my.hibernate.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class UserDto {
    private UUID id = UUID.randomUUID();
    private String name;
    private String email;
    private List<QuizResultDto> quizResults = new ArrayList<>();
}
