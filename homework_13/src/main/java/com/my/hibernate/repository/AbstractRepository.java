package com.my.hibernate.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Optional;

@Repository
public abstract class AbstractRepository<T> implements GenericRepository<T> {
    protected final SessionFactory sessionFactory;

    public AbstractRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public void save(T object) {
        sessionFactory.getCurrentSession().save(object);
    }

    @Override
    @Transactional
    public Optional<T> get(Class<T> clazz, Serializable id) {
        T object = sessionFactory.getCurrentSession().get(clazz, id);
        return Optional.of(object);
    }

    @Override
    @Transactional
    public void delete(Class<T> clazz, Serializable id) {
        T object = get(clazz, id)
                .orElseThrow(RuntimeException::new);
        sessionFactory.getCurrentSession().delete(object);
    }
}
