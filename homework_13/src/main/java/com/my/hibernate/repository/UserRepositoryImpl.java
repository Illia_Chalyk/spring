package com.my.hibernate.repository;

import com.my.hibernate.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {
    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Transactional
    public Optional<User> get(String email) {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session
                .getNamedQuery("User_getByEmail")
                .setParameter("email", email)
                .getSingleResult();
        return Optional.of(user);
    }
}
