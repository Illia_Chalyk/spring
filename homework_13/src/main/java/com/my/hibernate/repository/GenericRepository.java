package com.my.hibernate.repository;

import java.io.Serializable;
import java.util.Optional;

public interface GenericRepository<T> {
    void save(T object);

    Optional<T> get(Class<T> clazz, Serializable id);

    void delete(Class<T> clazz, Serializable id);
}
