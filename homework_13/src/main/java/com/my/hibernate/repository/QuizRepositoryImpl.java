package com.my.hibernate.repository;

import com.my.hibernate.entity.Quiz;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public class QuizRepositoryImpl extends AbstractRepository<Quiz> implements QuizRepository {
    @Autowired
    public QuizRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    @Transactional
    public List<Quiz> getByIds(List<UUID> ids) {
        Session session = sessionFactory.getCurrentSession();
        List<Quiz> quizzes = session.byMultipleIds(Quiz.class).multiLoad(ids);
        return quizzes;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Quiz> getAll() {
        List<Quiz> result = sessionFactory
                .getCurrentSession()
                .createQuery("from Quiz", Quiz.class)
                .list();
        return result;
    }
}
