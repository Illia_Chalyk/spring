package com.my.mongo;

import com.my.mongo.entity.Account;
import com.my.mongo.entity.Address;
import com.my.mongo.entity.Customer;
import com.my.mongo.repository.CustomerRepository;
import com.my.mongo.service.CustomerService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootTest
class CustomerServiceIT {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CustomerService sut;

    @AfterEach
    public void cleanUp() {
        customerRepository.deleteAll();
    }

    @Test
    public void shouldCreateNewCustomer() {
        //GIVEN
        Customer expected = getMockCustomer();
        //WHEN
        sut.insert(expected);
        Customer actual = sut.findById(expected.getId());
        //THEN
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void shouldNotFindUnexistingCustomer() {
        Assertions.assertThrows(RuntimeException.class, () -> sut.findById(20_000));
    }

    @Test
    public void shouldUpdateExistingCustomer() {
        //GIVEN
        Customer customer = getMockCustomer();
        String newFirstName = "newFirstName";
        sut.insert(customer);
        //WHEN
        customer.setFirstName(newFirstName);
        sut.updateOrInsert(customer);
        //THEN
        Customer fromDb = sut.findById(customer.getId());
        Assertions.assertEquals(newFirstName, fromDb.getFirstName());
    }

    @Test
    public void shouldFindAllCustomersWithExpiredAccounts() {
        //GIVEN
        int numOfExpiredAccounts = 5;
        List<Customer> toAdd = new ArrayList<>();
        for (int i = 0; i < numOfExpiredAccounts; i++) {
            Customer expired = getMockCustomerWithExpiredAccount();
            toAdd.add(expired);
        }
        toAdd.add(getMockCustomerWithValidAccount());
        toAdd.add(getMockCustomerWithValidAccount());
        toAdd.add(getMockCustomerWithValidAccount());
        customerRepository.saveAll(toAdd);
        //WHEN
        List<Customer> actual = sut.findAllCustomersWithExpiredAccounts();
        //THEN
        Assertions.assertEquals(numOfExpiredAccounts, actual.size());

    }

    @Test
    public void shouldFindCustomersByFirstName() {
        //GIVEN
        Customer expected = getMockCustomer();
        sut.insert(expected);
        //WHEN
        Customer actual = sut.findAllByFirstName(expected.getFirstName()).get(0);
        //THEN
        Assertions.assertEquals(expected.getFirstName(), actual.getFirstName());
    }

    @Test
    public void shouldFindCustomersByLastName() {
        //GIVEN
        Customer expected = getMockCustomer();
        sut.insert(expected);
        //WHEN
        Customer actual = sut.findAllByLastName(expected.getLastName()).get(0);
        //THEN
        Assertions.assertEquals(expected.getLastName(), actual.getLastName());
    }

    @Test
    public void shouldFindCustomersByAddress() {
        //GIVEN
        Customer expected = getMockCustomer();
        sut.insert(expected);
        //WHEN
        Customer actual = sut.findAllByAddress(expected.getAddresses().get(0)).get(0);
        //THEN
        Assertions.assertEquals(expected.getAddresses(), actual.getAddresses());
    }

    @Test
    public void shouldFindCustomersByCardNumber() {
        //GIVEN
        Customer expected = getMockCustomer();
        sut.insert(expected);
        //WHEN
        Customer actual = sut.findByCardNumber(expected.getAccounts().get(0).getCardNumber());
        //THEN
        Assertions.assertEquals(expected.getAccounts().get(0).getCardNumber(), actual.getAccounts().get(0).getCardNumber());
    }

    private Customer getMockCustomerWithValidAccount() {
        Customer customer = new Customer();
        customer.setId((int) ((Math.random() * 10_000) + 1));
        customer.setFirstName(RandomString.make());
        customer.setLastName(RandomString.make());
        customer.setAddresses(List.of(getMockAddress()));
        customer.setAccounts(List.of(getValidMockAccount()));
        return customer;
    }

    private Account getValidMockAccount() {
        Random random = new Random();
        Account account = new Account();
        account.setNameOnAccount(RandomString.make());
        account.setExpirationDate(YearMonth.of(random.nextInt(100) + 2022, random.nextInt(12) + 1));
        account.setCardNumber(RandomString.make(16));
        return account;

    }

    private Customer getMockCustomerWithExpiredAccount() {
        Customer customer = new Customer();
        customer.setId((int) ((Math.random() * 10_000) + 1));
        customer.setFirstName(RandomString.make());
        customer.setLastName(RandomString.make());
        customer.setAddresses(List.of(getMockAddress()));
        customer.setAccounts(List.of(getExpiredMockAccount()));
        return customer;
    }

    private Account getExpiredMockAccount() {
        Random random = new Random();
        Account account = new Account();
        account.setNameOnAccount(RandomString.make());
        account.setExpirationDate(YearMonth.of(random.nextInt(2021), random.nextInt(12) + 1));
        account.setCardNumber(RandomString.make(16));
        return account;
    }


    private Customer getMockCustomer() {
        Customer customer = new Customer();
        customer.setId((int) ((Math.random() * 10_000) + 1));
        customer.setFirstName(RandomString.make());
        customer.setLastName(RandomString.make());
        customer.setAddresses(List.of(getMockAddress()));
        customer.setAccounts(List.of(getMockAccount()));
        return customer;
    }

    private Address getMockAddress() {
        Address address = new Address();
        address.setCountryCode(RandomString.make(2).toUpperCase());
        address.setLine1(RandomString.make());
        address.setLine2(RandomString.make());
        return address;
    }

    private Account getMockAccount() {
        Random random = new Random();
        Account account = new Account();
        account.setNameOnAccount(RandomString.make());
        account.setExpirationDate(YearMonth.of(random.nextInt(2100), random.nextInt(12) + 1));
        account.setCardNumber(RandomString.make(16));
        return account;
    }


}
