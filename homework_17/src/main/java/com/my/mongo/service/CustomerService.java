package com.my.mongo.service;

import com.my.mongo.entity.Address;
import com.my.mongo.entity.Customer;
import com.my.mongo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.YearMonth;
import java.util.List;

@Service
public class CustomerService {
    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public void insert(Customer customer) {
        repository.insert(customer);
    }

    public void updateOrInsert(Customer customer) {
        repository.save(customer);
    }

    public Customer findById(Integer id) {
        return repository.findById(id).orElseThrow(() -> {
            throw new RuntimeException("Customer with id: " + id + " not found");
        });
    }

    public List<Customer> findAllCustomersWithExpiredAccounts() {
        return repository.findAllByAccountsExpirationDateBefore(YearMonth.now());
    }

    public List<Customer> findAllByFirstName(String firstName) {
        return repository.findAllByFirstName(firstName);
    }


    public List<Customer> findAllByLastName(String lastName) {
        return repository.findAllByLastName(lastName);
    }

    public List<Customer> findAllByAddress(Address address) {
        return repository.findAllByAddresses(address);
    }

    public Customer findByCardNumber(String cardNumber) {
        return repository.findByAccountsCardNumber(cardNumber).orElseThrow(() -> {
            throw new RuntimeException("Customer with card number: " + cardNumber + " not found");
        });
    }
}
