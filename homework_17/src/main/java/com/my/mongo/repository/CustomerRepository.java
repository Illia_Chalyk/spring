package com.my.mongo.repository;

import com.my.mongo.entity.Address;
import com.my.mongo.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends MongoRepository<Customer, Integer> {
    List<Customer> findAllByAccountsExpirationDateBefore(YearMonth date);

    List<Customer> findAllByFirstName(String firstName);

    List<Customer> findAllByLastName(String lastName);

    List<Customer> findAllByAddresses(Address address);

    Optional<Customer> findByAccountsCardNumber(String cardNumber);
}
