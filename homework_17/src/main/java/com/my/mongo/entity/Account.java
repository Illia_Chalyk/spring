package com.my.mongo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.YearMonth;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private String cardNumber;
    private String nameOnAccount;
    private YearMonth expirationDate;
}
