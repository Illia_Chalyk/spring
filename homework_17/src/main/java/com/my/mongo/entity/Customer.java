package com.my.mongo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("customers")
public class Customer {
    @Id
    private Integer id;
    private String firstName;
    private String lastName;
    private List<Address> addresses;
    private List<Account> accounts;
}


