package com.my.mongo.cfg;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

import java.time.YearMonth;

@ReadingConverter
public class YearMonthReadConverter implements Converter<Document, YearMonth> {
    @Override
    public YearMonth convert(Document source) {
        return YearMonth.of(
                source.get("year", Integer.class),
                source.get("month", Integer.class)
        );
    }
}

