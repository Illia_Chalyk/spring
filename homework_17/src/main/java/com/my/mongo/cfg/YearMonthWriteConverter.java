package com.my.mongo.cfg;

import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import java.time.YearMonth;

@WritingConverter
public class YearMonthWriteConverter implements Converter<YearMonth, Document> {

    @Override
    public Document convert(YearMonth source) {
        Document document = new Document();
        document.put("year", source.getYear());
        document.put("month", source.getMonth().getValue());
        return document;
    }

}

