package com.my.mongo.cfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.ArrayList;
import java.util.List;

@Configuration
class MongoCustomConversionsConfiguration {
    @Bean
    protected MongoCustomConversions configureConverters() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new com.my.mongo.cfg.YearMonthReadConverter());
        converters.add(new com.my.mongo.cfg.YearMonthWriteConverter());
        return new MongoCustomConversions(converters);
    }
}

