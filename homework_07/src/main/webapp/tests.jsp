<%@include file="page.jspf" %>
<%@include file="taglib.jspf" %>
<!DOCTYPE html>
<html>
<c:set var="title" value="All tests" scope="page"/>
<%@include file="head.jspf" %>
<body>
<%@include file="header.jspf" %>
<div class="container">
<nav>
    <%--  TODO add sorting and filtering methods  --%>
    <c:if test="${user.role.toString().equals('ADMIN')}">
        <div align="center">
            <a href="addtest">Add a new test</a><br>
                <%--            <a href="#">To editing tests</a><br>--%>
        </div>
    </c:if>
</nav>

<!--------------------------------------------------------------------------------------------------->


<h2 align="center">All tests</h2>
<c:forEach var="test" items="${tests}">
<div class="card mb-3">
    <div class="row g-0">
        <div class="col-1 bg-primary"></div>
        <div class="col-11">
            <div class="card-body">
                <h5 class="card-title display-6 mb-4 mt-2">${test.name}</h5>
                <p class="card-text"><span class="fw-bold">Subject: </span>${test.subject}</p>
                <p class="card-text"><span class="fw-bold">Time limit in minutes: </span>${test.timeLimitInMinutes}</p>
                <p class="card-text"><span class="fw-bold">Number of questions: </span>${test.numberOfQuestions}</p>
                <p class="card-text"><span class="fw-bold">Description: </span>${test.description}</p>
                <a href="#" class="btn btn-primary">Pass the test</a>
            </div>
        </div>
    </div>
</div>
</c:forEach>

<%--<div>--%>
<%--    <c:forEach var="test" items="${tests}">--%>
<%--        <div class="card">--%>
<%--            <h3>${test.name}</h3>--%>
<%--            <span>Subject: ${test.subject}</span><br>--%>
<%--            <span>Test difficulty: ${test.difficulty}</span><br>--%>
<%--            <span>Number of questions: ${test.numberOfQuestions}</span><br>--%>
<%--            <a href="info?testId=${test.id}">More info...</a>--%>
<%--        </div>--%>
<%--    </c:forEach>--%>
<%--</div>--%>

<br>
<nav align="center">
    <%-- TODO add sorting method --%>
    <ul>
        <c:if test="${currentPage gt 1}">
            <a href="tests?recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}">Previous</a>
        </c:if>
        <c:forEach var="i" begin="1" end="${numOfPages}">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <a>(${i})</a>
                </c:when>
                <c:otherwise>
                    <a href="tests?recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage lt numOfPages}">
            <a href="tests?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}">Next</a>
        </c:if>
    </ul>
</nav>
</div>
</div>
</body>
</html>
