package com.my.spring.model.user;

public enum Permission {
    USER("user_permission"),
    ADMIN("admin_permission");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
