package com.my.spring.model.test;

import java.util.Objects;

public class QuestionAnswerPair {
    private int questionId;
    private int answerId;

    public QuestionAnswerPair(int questionId, int answerId) {
        this.questionId = questionId;
        this.answerId = answerId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionAnswerPair pair = (QuestionAnswerPair) o;
        return questionId == pair.questionId && answerId == pair.answerId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionId, answerId);
    }
}
