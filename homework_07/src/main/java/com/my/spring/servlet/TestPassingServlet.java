package com.my.spring.servlet;

import com.my.spring.dao.DAOException;
import com.my.spring.dao.DAOFactory;
import com.my.spring.dao.TestDAO;
import com.my.spring.dao.TestResultDAO;
import com.my.spring.dao.rdb.RdbDAOFactory;
import com.my.spring.model.test.QuestionAnswerPair;
import com.my.spring.model.test.Test;
import com.my.spring.model.test.TestResult;
import com.my.spring.model.user.User;
import com.my.spring.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Didn't rewrite this servlet because the logic has some issues that need to be deep refactored
 */
//TODO find better way to store test info than session!!!
public class TestPassingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        //FIXME if user tries to pass test
        // while previous test still in the session
        // user sees questions for the previous test
        // though user opens another test
        if (req.getSession().getAttribute("test") == null) {
            initTest(req, res);
        }

        int currentQuestion = 0;
        if (req.getSession().getAttribute("currentQuestion") != null) {
            currentQuestion = (Integer) (req.getSession().getAttribute("currentQuestion"));
        }
        if (req.getParameter("next") != null) {
            //TODO validation
            currentQuestion++;
        }
        if (req.getParameter("previous") != null) {
            //TODO validation
            currentQuestion--;
        }
        req.getSession().setAttribute("currentQuestion", currentQuestion);

        //FIXME suspect this to cause bugs
        req.getRequestDispatcher("testpassing.jsp").forward(req, res);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Timer timer = (Timer) session.getAttribute("timer");
        if (!timer.isActive() || req.getParameter("submitTest") != null) {
            submitTest(req, res);
            return;
        }

        Set<QuestionAnswerPair> pairSet = (Set<QuestionAnswerPair>) session.getAttribute("pairSet");

        //TODO replace parameters to sessions attributes
        int testId = Integer.parseInt(req.getParameter("testId"));
        int questionId = Integer.parseInt(req.getParameter("questionId"));
        String[] answers = req.getParameterValues("ans");

        if (answers != null) {
            for (String ans : req.getParameterValues("ans")) {
                int answerId = Integer.parseInt(ans);
                pairSet.add(new QuestionAnswerPair(questionId, answerId));
            }
        }
        //TODO save the answer to the db and sendRedirect to the next question
        // or if a question is the last then submit the test
        res.sendRedirect("#"); //TODO refactor this
    }

    void initTest(HttpServletRequest req, HttpServletResponse res) {
        //TODO  1) refactor this
        //      2) insert into test_result table in db
        int testId = Integer.parseInt(req.getParameter("testId"));
        Test test = null;
        TestResult testResult = new TestResult();

        DAOFactory factory = RdbDAOFactory.getInstance();
        TestDAO testDAO = factory.getTestDAO();
        TestResultDAO testResultDAO = factory.getTestResultDAO();

        try {
            test = testDAO.getTestById(testId);
            testResult.setTestId(test.getId());
            int userId = ((User) req.getSession().getAttribute("user")).getId();
            testResult.setUserId(userId);
            testResultDAO.insertTestResult(testResult);

        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }

        HttpSession session = req.getSession();
        Timer timer = new Timer(test.getTimeLimitInMinutes());
        Set<QuestionAnswerPair> pairSet = new HashSet<>();
        session.setAttribute("test", test);
        session.setAttribute("pairSet", pairSet);
        session.setAttribute("attemptId", testResult.getId());
        session.setAttribute("timer", timer);
    }

    void submitTest(HttpServletRequest req, HttpServletResponse res) throws IOException {
        //TODO wrap this attributes into one class
        HttpSession session = req.getSession();
        Test test = (Test) session.getAttribute("test");
        Set<QuestionAnswerPair> pairSet = (Set<QuestionAnswerPair>) session.getAttribute("pairSet");
        //TODO could use this to check how mush time test took
        session.getAttribute("timer");
        session.getAttribute("numOfQuestions");
        session.getAttribute("currentQuestion");
        int attemptId = (Integer) session.getAttribute("attemptId");

        //TODO insert answers into db
        DAOFactory factory = RdbDAOFactory.getInstance();
        TestResultDAO testResultDAO = factory.getTestResultDAO();
        try {
            testResultDAO.insertUserAnswers(attemptId, pairSet);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        //After all work is done (clean up)
        session.removeAttribute("test");
        session.removeAttribute("pairSet");
        session.removeAttribute("timer");
        session.removeAttribute("numOfQuestions");
        session.removeAttribute("currentQuestion");
        session.removeAttribute("attemptId");

        //TODO pass attemptId
        res.sendRedirect("testresult?attemptId=" + attemptId);
    }
}
