package com.my.spring.dao;


public interface DAOFactory {
    UserDAO getUserDAO();

    TestDAO getTestDAO();

    TestResultDAO getTestResultDAO();
}
