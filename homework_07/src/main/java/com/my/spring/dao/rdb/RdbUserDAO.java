package com.my.spring.dao.rdb;

import com.my.spring.dao.DAOException;
import com.my.spring.dao.UserDAO;
import com.my.spring.model.user.Role;
import com.my.spring.model.user.Status;
import com.my.spring.model.user.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RdbUserDAO implements UserDAO {
    @Override
    public void insertUser(User user) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO sql constants
            statement = connection.prepareStatement("INSERT INTO `user` " +
                    "(first_name, last_name, email, password) " +
                    "VALUES (?, ?, ?, ?)");
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
    }

    @Override
    public User getUserByEmail(String email) throws DAOException {
        User user = new User();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            //TODO  1) sql constants
            //      2) add all user fields to query
            statement = connection.prepareStatement("SELECT * FROM user WHERE email = ?");
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                //TODO exception handling
                throw new DAOException("User not found");
            }
            mapUser(user, resultSet);
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return user;
    }

    private void mapUser(User user, ResultSet resultSet) throws SQLException {
        user.setId(resultSet.getInt("id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        int roleId = resultSet.getInt("role") - 1;
        user.setRole(Role.values()[roleId]);
        int statusId = resultSet.getInt("status") - 1;
        user.setStatus(Status.values()[statusId]);
        user.setRegistrationDate(resultSet.getDate("registration_date"));
    }

    /**
     * Meant to check if user already registered.
     *
     * @return num of found users.
     * @throws DAOException
     */
    @Override
    public boolean isUserExist(User user) throws DAOException {
        //TODO  1) add this to generic DAO with additional parameters
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        //TODO sql constants
        try {
            connection = RdbDAOFactory.getInstance().getConnection();
            statement = connection.prepareStatement("SELECT COUNT(email) AS count FROM user WHERE email = ?");
            statement.setString(1, user.getEmail());
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count += resultSet.getInt("count");
            }
        } catch (SQLException e) {
            //TODO exception handling
            e.printStackTrace();
        } finally {
            RdbDAOFactory.close(resultSet);
            RdbDAOFactory.close(statement);
            RdbDAOFactory.close(connection);
        }
        return count == 0;
    }
}
