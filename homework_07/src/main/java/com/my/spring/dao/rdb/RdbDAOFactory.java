package com.my.spring.dao.rdb;

import com.my.spring.dao.DAOFactory;
import com.my.spring.dao.TestDAO;
import com.my.spring.dao.TestResultDAO;
import com.my.spring.dao.UserDAO;
import org.springframework.stereotype.Component;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Component
public class RdbDAOFactory implements DAOFactory {
    private static RdbDAOFactory instance;

    private RdbDAOFactory() {
        if (instance != null) {
            throw new AssertionError("Do not instantiate the class twice!");
        }
    }

    public static RdbDAOFactory getInstance() {
        if (instance == null) {
            synchronized (RdbDAOFactory.class) {
                if (instance == null) {
                    instance = new RdbDAOFactory();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = null;
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/testing");
            connection = ds.getConnection();
        } catch (NamingException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        return connection;
    }

    public static void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
                //TODO exception handling
                e.printStackTrace();
            }
        }
    }

    @Override
    public UserDAO getUserDAO() {
        return new RdbUserDAO();
    }

    @Override
    public TestDAO getTestDAO() {
        return new RdbTestDAO();
    }

    @Override
    public TestResultDAO getTestResultDAO() {
        return new RdbTestResultDAO();
    }
}
