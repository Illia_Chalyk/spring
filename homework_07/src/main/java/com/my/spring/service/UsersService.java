package com.my.spring.service;

import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

public interface UsersService {
    void registerNewUser(String firstName,
                         String lastName,
                         String email,
                         String password);

    void getUserTestResults(Model model, HttpSession session);
}
