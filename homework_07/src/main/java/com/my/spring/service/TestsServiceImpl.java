package com.my.spring.service;

import com.my.spring.dao.DAOException;
import com.my.spring.dao.TestDAO;
import com.my.spring.model.test.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class TestsServiceImpl implements TestsService {
    @Autowired
    TestDAO testDAO;


    @Override
    public void getTests(Model model, String currentPage) {
        List<Test> tests = null;
        int recordsPerPage = 10;
        int cur;
        if (currentPage == null) {
            cur = 1;
        } else {
            cur = Integer.parseInt(currentPage);
        }
        int numOfPages = 0;
        try {
            tests = testDAO.getAllTests(recordsPerPage,
                    cur * recordsPerPage - recordsPerPage);
            int numOfTests = testDAO.getNumberOfTests();
            numOfPages = (int) Math.ceil((double) numOfTests / recordsPerPage);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        model.addAttribute("tests", tests);
        model.addAttribute("currentPage", cur);
        model.addAttribute("recordsPerPage", recordsPerPage);
        model.addAttribute("numOfPages", numOfPages);
    }

    @Override
    public void getTest(int testId, Model model) {
        Test test = null;
        try {
            test = testDAO.getTestById(testId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        model.addAttribute("test", test);
    }

    @Override
    public int addNewTest(String testName,
                          String subject,
                          String difficulty,
                          int timeLimitInMinutes,
                          String description) {
        Test test = new Test();
        test.setName(testName);
        test.setSubject(subject);
        test.setDifficulty(TestDifficulty.valueOf((difficulty.toUpperCase())));
        test.setTimeLimitInMinutes(timeLimitInMinutes);
        test.setDescription(description);

        try {
            testDAO.insertTest(test);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
        return test.getId();
    }

    @Override
    public void deleteTest(int testId) {
        try {
            testDAO.deleteTestDyId(testId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void updateTest(int testId,
                           String testName,
                           String subject,
                           String difficulty,
                           int timeLimitInMinutes,
                           String description,
                           String submit) {
        try {
            Test test = testDAO.getTestById(testId);
            if (submit != null) {
                test.setStatus(TestStatus.SUBMITTED);
            } else {
                test.setName(testName);
                test.setSubject(subject);
                test.setDifficulty(TestDifficulty.valueOf(difficulty.toUpperCase()));
                test.setTimeLimitInMinutes(timeLimitInMinutes);
                test.setDescription(description);
            }
            testDAO.updateTest(test);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void addQuestionToTest(String questionText, int testId) {
        Question question = new Question();
        question.setQuestionText(questionText);
        Test test = new Test();
        test.setId(testId);
        try {
            testDAO.addQuestionToTest(test, question);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void editQuestion(int questionId, String questionText) {
        try {
            Question question = testDAO.getQuestionById(questionId);
            question.setQuestionText(questionText);
            testDAO.updateQuestion(question);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void deleteQuestion(int questionId) {
        try {
            testDAO.deleteQuestionById(questionId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void abbAnswerToQuestion(String answerText,
                                    boolean correct,
                                    int questionId) {
        Answer answer = new Answer();
        answer.setAnswerText(answerText);
        answer.setCorrect(correct);
        try {
            Question question = testDAO.getQuestionById(questionId);
            testDAO.addAnswerToQuestion(question, answer);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void editAnswer(int answerId, boolean correct, String answerText) {
        try {
            Answer answer = testDAO.getAnswerById(answerId);
            answer.setAnswerText(answerText);
            answer.setCorrect(correct);
            testDAO.updateAnswer(answer);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAnswer(int answerId) {
        try {
            testDAO.deleteAnswerById(answerId);
        } catch (DAOException e) {
            //TODO exception handling
            e.printStackTrace();
        }
    }
}
