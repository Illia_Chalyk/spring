package com.my.messaging.receiver;

import com.my.messaging.message.RequestMessage;
import com.my.messaging.message.ResponseMessage;
import com.my.messaging.sender.MessageSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Service
public class MessageReceiver {
    public static final int EXPECTED_AMOUNT_OF_MESSAGES = 15;
    public static final String QUEUE = "queue";
    private final JmsTemplate jmsTemplate;
    private final List<RequestMessage> cache;
    private int counter;

    @Autowired
    public MessageReceiver(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
        this.cache = new ArrayList<>();
        this.counter = 0;
    }

    @JmsListener(destination = QUEUE)
    public void receiveReply(@Payload RequestMessage message) {
        log.info("Request received: {}", message);

        cache.add(message);
        counter++;

        if (counter >= EXPECTED_AMOUNT_OF_MESSAGES) {
            log.info("Limit of requests in cache reached. Processing all from cache...");
            sendAllFromCache();
            log.info("Cache was processed successfully.");
        }
    }

    private void sendAllFromCache() {
        for (RequestMessage m : cache) {
            log.info("Sending reply: {}", m);
            ResponseMessage response = new ResponseMessage();
            response.setCorrelationId(m.getCorrelationId());
            response.setResult(m.getFistArg() + m.getSecondArg());

            //imitate a failure while processing request
            Random random = new Random();
            if (random.nextInt(3) == 0) {
                log.error("Imitating failure...");
                if (random.nextInt(2) == 0) {
                    response.setResult(-1);
                } else {
                    response.setCorrelationId(UUID.randomUUID());
                }
            }

            jmsTemplate.convertAndSend(MessageSender.MESSAGE_REPLY, response);
        }
        log.info("Set counter to 0.");
        counter = 0;
        log.info("Clearing cache.");
        cache.clear();
    }
}
