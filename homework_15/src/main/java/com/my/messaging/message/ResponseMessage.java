package com.my.messaging.message;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@ToString
public class ResponseMessage implements Serializable {
    private int result;
    private UUID correlationId;
}
