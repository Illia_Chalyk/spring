package com.my.messaging.message;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@ToString
public class RequestMessage implements Serializable {
    private int fistArg;
    private int secondArg;
    private UUID correlationId = UUID.randomUUID();
}
