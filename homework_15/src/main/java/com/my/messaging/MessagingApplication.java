package com.my.messaging;

import com.my.messaging.message.RequestMessage;
import com.my.messaging.sender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class MessagingApplication implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(MessagingApplication.class, args);
    }

    @Autowired
    private MessageSender sender;

    @Override
    public void run(ApplicationArguments args) {
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            RequestMessage message = new RequestMessage();
            message.setFistArg(random.nextInt(1000));
            message.setSecondArg(random.nextInt(1000));
            sender.sendQueue(message);
        }
    }
}
