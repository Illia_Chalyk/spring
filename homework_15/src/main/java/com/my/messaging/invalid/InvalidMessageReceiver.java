package com.my.messaging.invalid;

import com.my.messaging.message.ResponseMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InvalidMessageReceiver {
    public static final String INVALID_MESSAGE_IDS = "invalid-message-ids";
    public static final String INVALID_MESSAGE_RESULTS = "invalid-message-results";
    private final JmsTemplate jmsTemplate;

    @Autowired
    public InvalidMessageReceiver(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = INVALID_MESSAGE_IDS)
    public void receiveInvalidMessageIds(@Payload ResponseMessage message) {
        log.error("Reply's message id is invalid.");
    }

    @JmsListener(destination = INVALID_MESSAGE_RESULTS)
    public void receiveInvalidMessageResults(@Payload ResponseMessage message) {
        log.error("Reply's message result is invalid.");
    }
}
