package com.my.messaging.sender;

import com.my.messaging.invalid.InvalidMessageReceiver;
import com.my.messaging.message.RequestMessage;
import com.my.messaging.message.ResponseMessage;
import com.my.messaging.receiver.MessageReceiver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Service
public class MessageSender {
    public static final String MESSAGE_REPLY = "message-reply";
    private final JmsTemplate jmsTemplate;
    private final Map<UUID, RequestMessage> cache;

    @Autowired
    public MessageSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
        this.cache = new HashMap<>();
    }

    public void sendQueue(RequestMessage message) {
        log.info("Sending: {}", message);
        cache.put(message.getCorrelationId(), message);
        jmsTemplate.convertAndSend(MessageReceiver.QUEUE, message);
    }

    @JmsListener(destination = MESSAGE_REPLY)
    public void receiveReply(@Payload ResponseMessage message) {
        log.info("Reply received: {}", message);
        if (!isResponseMessageIdExistInCache(message.getCorrelationId())) {
            jmsTemplate.convertAndSend(InvalidMessageReceiver.INVALID_MESSAGE_IDS, message);
            return;
        }
        if (!isResponseMessageResultValid(message)) {
            jmsTemplate.convertAndSend(InvalidMessageReceiver.INVALID_MESSAGE_RESULTS, message);
            return;
        }
        log.info("Reply is valid.");
    }

    private boolean isResponseMessageIdExistInCache(UUID id) {
        log.info("Checking reply message id...");
        return cache.containsKey(id);
    }

    private boolean isResponseMessageResultValid(ResponseMessage message) {
        log.info("Checking reply result...");
        RequestMessage requestMessage = cache.get(message.getCorrelationId());
        int expected = requestMessage.getFistArg() + requestMessage.getSecondArg();
        int actual = message.getResult();
        return expected == actual;
    }
}
